import { css } from 'lit';

export const userCardStyles = css`
  .user-card {
    width: 360px;
    height: 400px;
    border-radius: 24px;
    background-color: #f6f6f6;
    padding: 25px;
    box-sizing: border-box;
    cursor: pointer;
  }
  .user-card-details {
    display: flex;
    justify-content: space-between;
    margin-top: 16px;
  }
  .user-card-image {
    width: 310px;
    height: 285px;
    border-radius: 16px;
    background-color: #eeeeee;
    object-fit: cover;
    display: block;
  }
  .user-card-name {
    font-size: 20px;
    color: #202020;
    font-weight: 500;
  }
  .user-card-role {
    font-size: 14px;
    color: #7d7d7d;
  }
`;
