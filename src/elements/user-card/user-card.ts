import { LitElement, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { userCardStyles } from './user-card.styles';

@customElement('user-card')
export class UserCardElement extends LitElement {
  static styles = [userCardStyles];
  @property({ type: String })
  imageUrl: string = '';
  @property({ type: String })
  name: string = '';
  @property({ type: String })
  designation: string = '';

  render() {
    return html`
      <div class="user-card" @click=${this._handleCardClick}>
        <img class="user-card-image" src=${this.imageUrl} alt="${this.name}-image" />
        <div class="user-card-details">
          <div>
            <div class="user-card-name">${this.name}</div>
            <div class="user-card-role">${this.designation}</div>
          </div>
          <action-button @click=${this._handleAddClick} buttonIcon="add"> </action-button>
        </div>
      </div>
    `;
  }

  private _handleAddClick(event: Event) {
    event.stopPropagation();
    this.dispatchEvent(new CustomEvent('addClick'));
  }

  private _handleCardClick(event: Event) {
    event.stopPropagation();
    this.dispatchEvent(new CustomEvent('cardClick'));
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'user-card': UserCardElement;
  }
}
