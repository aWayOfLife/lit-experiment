import { LitElement, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { slotsViewerStyles } from './slots-viewer.styles';
import * as moment from 'moment';

export interface SlotViewerData {
  date?: moment.Moment;
  slots?: boolean[];
}

@customElement('slots-viewer')
export class SlotsViewerElement extends LitElement {
  static styles = [slotsViewerStyles];
  @property({ type: Object })
  slotViewerData: SlotViewerData = {};
  render() {
    return html`
      <div class="slots-viewer">
        <div class="date">${this.slotViewerData.date?.format('DD MMMM')}</div>
        <div class="weekday">${this.slotViewerData.date?.format('dddd')}</div>
        <div class="slots">${this.slotViewerData.slots?.map((isBooked: boolean, i: number) => this._getSlot(isBooked, i))}</div>
      </div>
    `;
  }
  private _getSlot(isBooked: boolean, i: number) {
    return html`
      <div class="slot">
        <span class="slot-divider ${i % 2 === 0 ? 'even' : 'odd'}" key=${i}></span>
        <div class="slot-details" key=${i}>
          <div class="slot-indicator ${isBooked ? 'booked' : ''}"></div>
          ${i % 2 === 0 ? html`<div class="slot-hour">${i}</div>` : null}
        </div>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'slots-viewer': SlotsViewerElement;
  }
}
