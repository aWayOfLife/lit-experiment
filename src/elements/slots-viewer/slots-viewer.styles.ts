import { css } from 'lit';

export const slotsViewerStyles = css`
  .slots-viewer {
    width: 360px;
  }
  .date {
    font-size: 20px;
    color: #202020;
    font-weight: 500;
  }
  .weekday {
    font-size: 14px;
    color: #7d7d7d;
  }
  .slots {
    margin-top: 8px;
    display: flex;
  }
  .slots .slot {
    display: flex;
  }
  .slot-divider {
    margin-top: 4px;
    border-left: 1px solid #dddddd;
    box-sizing: border-box;
    margin-right: 3px;
  }
  .slot-divider.even {
    height: 60px;
  }
  .slot-divider.odd {
    height: 30px;
  }
  .slot-details {
    margin-right: 3px;
    position: relative;
  }
  .slot-indicator {
    height: 42px;
    width: 8px;
    border-radius: 5px;
  }
  .slot-indicator.booked {
    background-color: #2caffe;
  }
  .slot-hour {
    font-size: 10px;
    color: #7d7d7d;
    position: absolute;
    bottom: -4px;
  }
`;
