import { css } from 'lit';

export const actionButtonStyles = css`
  .action-button {
    width: 42px;
    height: 42px;
    border-radius: 21px;
    background-color: #2caffe;
    display: flex;
    justify-content: center;
    align-items: center;
    border: unset;
    cursor: pointer;
  }
  .action-button-icon {
    color: white;
    font-size: 24px;
  }
`;
