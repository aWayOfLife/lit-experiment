import { LitElement, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { actionButtonStyles } from './action-button.styles';

@customElement('action-button')
export class ActionButtonElement extends LitElement {
  static styles = [actionButtonStyles];
  @property({ type: String })
  buttonIcon: string = '';
  render() {
    return html`
      <button class="action-button">
        <mat-icon class="action-button-icon" icon=${this.buttonIcon}></mat-icon>
      </button>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'action-button': ActionButtonElement;
  }
}
