import { LitElement, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { bottomNavStyles } from './bottom-nav.styles';

export interface BottomNavMenuItem {
  icon: string;
  isActive: boolean;
}

@customElement('bottom-nav')
export class BottomNavElement extends LitElement {
  static styles = [bottomNavStyles];
  @property({ type: Array })
  menuItems: BottomNavMenuItem[] = [];
  render() {
    return html`
      <div class="bottom-nav">
        <div class="bottom-nav-menu-items">${this.menuItems.map((menuItem, i) => this._generateMenuItemTemplate(menuItem, i))}</div>
      </div>
    `;
  }

  private _generateMenuItemTemplate(menuItem: BottomNavMenuItem, index: number) {
    return html` <mat-icon key=${index} class=${menuItem.isActive ? 'active' : ''} @click=${this._handleMenuItemClick} icon=${menuItem.icon}></mat-icon> `;
  }

  private _handleMenuItemClick(event: Event) {
    event.stopPropagation();
    this.dispatchEvent(
      new CustomEvent('bottomNavActiveMenuItemChange', {
        detail: {
          activeItemIndex: (event.target as Element).getAttribute('key'),
        },
      }),
    );
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'bottom-nav': BottomNavElement;
  }
}
