import { css } from 'lit';

export const bottomNavStyles = css`
  .bottom-nav {
    height: 72px;
    width: 360px;
    display: grid;
    place-items: center;
    border-radius: 24px;
    background-color: #f6f6f6;
    box-sizing: border-box;
    padding: 0 48px;
  }
  .bottom-nav-menu-items {
    display: flex;
    justify-content: space-between;
    width: 100%;
  }
  .bottom-nav-menu-items mat-icon {
    font-size: 20px;
    padding-bottom: 4px;
    cursor: pointer;
    color: #cccccc;
    position: relative;
  }
  .bottom-nav-menu-items mat-icon::after {
    background: none no-repeat scroll 0 0 transparent;
    bottom: 0;
    content: '';
    display: block;
    height: 2px;
    left: 50%;
    position: absolute;
    background: #2caffe;
    transition: width 0.3s ease 0s, left 0.3s ease 0s;
    width: 0;
  }
  .bottom-nav-menu-items mat-icon.active {
    color: #202020;
  }
  .bottom-nav-menu-items mat-icon.active::after {
    width: 100%;
    left: 0;
  }
`;
