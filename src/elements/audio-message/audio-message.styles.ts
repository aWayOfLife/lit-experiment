import { css } from 'lit';

export const audioMessageStyles = css`
  .audio-message {
    background-color: #f6f6f6;
    width: 360px;
    height: 72px;
    border-radius: 36px;
    padding: 15px;
    display: flex;
    gap: 16px;
    align-items: center;
    box-sizing: border-box;
  }
  .waveform-container {
    position: relative;
    height: 42px;
    width: 264px;
  }
  .waveform-container .waveform {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  svg {
    clip-path: url(#waveformPath);
  }
`;
