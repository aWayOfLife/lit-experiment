import { LitElement, html, svg } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { audioMessageStyles } from './audio-message.styles';

export interface AudioMessageData {
  duration?: number;
  elapsed?: number;
  levels?: number[];
  isPlaying?: boolean;
}
@customElement('audio-message')
export class AudioMessageElement extends LitElement {
  static styles = [audioMessageStyles];

  @property({ type: Object })
  audioMessageData: AudioMessageData | undefined;

  timer: number | undefined;

  render() {
    return html`
      <div class="audio-message">
        <action-button @click=${this._handlePlayToggle} buttonIcon=${this.audioMessageData?.isPlaying ? 'pause' : 'play_arrow'}></action-button>
        <div class="waveform-container">
          <svg height="42px" width="100%">
            <rect x="0" y="0" stroke="none" fill="#cccccc" width="100%" height="100%" />
            <rect x="0" y="0" stroke="none" fill="#2caffe" width="${this._getPercentageElapsed(this.audioMessageData?.duration || 1, this.audioMessageData?.elapsed || 0)}%" height="100%" />
            <defs>
              <clipPath class="waveform" id="waveformPath">${this.audioMessageData?.levels?.map((level: number, i: number) => this._getWave(level, i))}</clipPath>
            </defs>
          </svg>
        </div>
      </div>
    `;
  }

  private _handlePlayToggle(event: Event) {
    event.stopPropagation();
    this.dispatchEvent(new CustomEvent('playToggle'));
    if (this.audioMessageData?.isPlaying) {
      clearInterval(this.timer);
    } else {
      this.timer = setInterval(() => {
        this.audioMessageData = { ...this.audioMessageData, elapsed: (this.audioMessageData?.elapsed ?? 0) + 1 };
        if ((this.audioMessageData?.elapsed ?? 0) >= (this.audioMessageData?.duration ?? 0)) {
          clearInterval(this.timer);
          this.audioMessageData = { ...this.audioMessageData, elapsed: 0 };
          this.audioMessageData = { ...this.audioMessageData, isPlaying: !this.audioMessageData?.isPlaying };
        }
      }, 100);
    }
    this.audioMessageData = { ...this.audioMessageData, isPlaying: !this.audioMessageData?.isPlaying };
  }

  private _getWave(level: number, index: number) {
    return svg`<rect key=${index} class='wave' x="${index * 6}" y="${(42 - this._getWaveHeightFromLevel(level)) / 2}px" fill="#ffffff" width="4px" height="${this._getWaveHeightFromLevel(level)}px" rx="2"/>`;
  }

  private _getPercentageElapsed(duration: number, elapsed: number) {
    return (elapsed / duration) * 100;
  }

  private _getWaveHeightFromLevel(level: number) {
    return (level / 8) * 42;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'audio-message': AudioMessageElement;
  }
}
