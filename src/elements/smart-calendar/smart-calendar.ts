import { LitElement, PropertyValues, html, unsafeCSS } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import smartCalendarstyles from './smart-calendar.styles.scss?inline';
import { add, eachDayOfInterval, endOfMonth, format, getDay, isSameDay, startOfMonth, startOfToday, sub } from 'date-fns';

export interface SmartCalendarDate {
  day: Date;
  isSelectedDay: boolean;
  isActivityDay: boolean;
}
@customElement('smart-calendar')
export class SmartCalendarElement extends LitElement {
  static styles = unsafeCSS(smartCalendarstyles);

  @state()
  firstDayOfMonth: Date = startOfMonth(startOfToday());

  @state()
  selectedDay: Date | undefined;

  @state()
  smartCalendarDays: SmartCalendarDate[] = [];

  @property({
    type: Array,
    attribute: true,
    hasChanged(value: Date[], oldValue: Date[]) {
      return value?.length !== oldValue?.length;
    },
  })
  activityDays: Date[] = [];

  connectedCallback() {
    super.connectedCallback();
    this.setSmartCalendarDays();
  }

  willUpdate(changedProperties: PropertyValues<this>) {
    if (changedProperties.has('activityDays') || changedProperties.has('smartCalendarDays')) {
      this.mapActivityDays();
    }
  }

  render() {
    return html`
      <div class="smart-calendar">
        <div class="calendar-header">
          <div class="month-year-wrapper">
            <div class="month">${format(this.firstDayOfMonth, 'MMMM')}</div>
            <div class="year">${format(this.firstDayOfMonth, 'yyyy')}</div>
          </div>
          <div class="month-toggle-wrapper">
            <div @click=${this.handlePreviousMonth} class="month-toggle">
              <mat-icon icon="chevron_left"></mat-icon>
            </div>
            <div @click=${this.handleNextMonth} class="month-toggle">
              <mat-icon icon="chevron_right"></mat-icon>
            </div>
          </div>
        </div>
        <div class="calendar-body">
          <div class="weekday">Su</div>
          <div class="weekday">Mo</div>
          <div class="weekday">Tu</div>
          <div class="weekday">We</div>
          <div class="weekday">Th</div>
          <div class="weekday">Fr</div>
          <div class="weekday">Sa</div>

          ${this.smartCalendarDays?.map((day) => html`<smart-calendar-day @dayClick=${this.handleDayClick} .key=${day} .smartCalendarDay=${day}></smart-calendar-day>`)}
        </div>
      </div>
    `;
  }

  handleDayClick(event: CustomEvent) {
    this.selectedDay = event.detail.day;
    this.smartCalendarDays = [...this.smartCalendarDays.map((day) => (day ? { ...day, isSelectedDay: isSameDay(day.day, this.selectedDay as Date) } : undefined))] as unknown as SmartCalendarDate[];
  }

  handlePreviousMonth() {
    this.firstDayOfMonth = sub(this.firstDayOfMonth, { months: 1 });
    this.setSmartCalendarDays();
  }

  handleNextMonth() {
    this.firstDayOfMonth = add(this.firstDayOfMonth, { months: 1 });
    this.setSmartCalendarDays();
  }

  setSmartCalendarDays() {
    const unshiftBasedOnStartingDay = new Array(getDay(this.firstDayOfMonth)).fill(undefined);
    const calendarDays: SmartCalendarDate[] = eachDayOfInterval({ start: this.firstDayOfMonth, end: endOfMonth(this.firstDayOfMonth) }).map((day) => ({ day, isSelectedDay: this.selectedDay ? isSameDay(day, this.selectedDay as Date) : false, isActivityDay: false }));
    this.smartCalendarDays = [...unshiftBasedOnStartingDay.concat(calendarDays)];
  }

  mapActivityDays() {
    this.smartCalendarDays = [...this.smartCalendarDays.map((day) => (day ? { ...day, isActivityDay: !!this.activityDays.find((activityDay) => isSameDay(activityDay, day.day)) } : undefined))] as unknown as SmartCalendarDate[];
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'smart-calendar': SmartCalendarElement;
  }
}
