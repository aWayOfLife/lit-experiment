import { LitElement, html, unsafeCSS } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import smartCalendarDaystyles from './smart-calendar-day.styles.scss?inline';
import { format, isAfter, isBefore, isToday, startOfToday } from 'date-fns';
import { SmartCalendarDate } from '../smart-calendar';

@customElement('smart-calendar-day')
export class SmartCalendarDayElement extends LitElement {
  static styles = unsafeCSS(smartCalendarDaystyles);
  @property({
    type: Object,
  })
  smartCalendarDay: SmartCalendarDate | undefined;

  render() {
    return html` <div class="smart-calendar-day ${this.getClasses(this.smartCalendarDay?.day)}" @click=${() => this.handleDayClick(this.smartCalendarDay?.day as Date)}>${this.smartCalendarDay ? format(this.smartCalendarDay?.day as Date, 'd') : ''} ${this.smartCalendarDay?.isActivityDay ? html` <smart-calendar-day-scratch></smart-calendar-day-scratch> ` : null}</div> `;
  }

  getClasses(day: Date | undefined): string {
    let classes = '';
    if (!day) {
      classes += 'blank-day';
    }
    if (isToday(day as Date)) {
      classes += ' current-day';
    }
    if (isAfter(day as Date, startOfToday())) {
      classes += ' future-day';
    }
    if (isBefore(day as Date, startOfToday())) {
      classes += ' past-day';
    }
    if (this.smartCalendarDay?.isSelectedDay) {
      classes += ' selected-day';
    }
    if (this.smartCalendarDay?.isActivityDay) {
      classes += ' activity-day';
    }
    return classes;
  }

  handleDayClick(day: Date) {
    this.dispatchEvent(
      new CustomEvent('dayClick', {
        detail: {
          day,
        },
      }),
    );
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'smart-calendar-day': SmartCalendarDayElement;
  }
}
