import { LitElement, svg, unsafeCSS } from 'lit';
import { customElement } from 'lit/decorators.js';
import smartCalendarDayScratchstyles from './smart-calendar-day-scratch.styles.scss?inline';
@customElement('smart-calendar-day-scratch')
export class SmartCalendarDayScratchElement extends LitElement {
  static styles = unsafeCSS(smartCalendarDayScratchstyles);

  render() {
    return svg`
        <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" transform="translate(-11 -11)rotate(180)">
            <path class="scratch" stroke="#202020" d="M554.684,638.454l-9.919,4.838,11.974-11.538s-17.416,9.56-19.23,7.912,14.151-11.208,14.151-11.208-13.425,8.571-14.151,6.263,15-11.429,15-11.429-11.276,2.39-15,5.166,7.257,1.459,7.257,1.459" transform="translate(-535.729 -622.292)" fill="none" stroke="#202020" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
        </svg>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'smart-calendar-day-scratch': SmartCalendarDayScratchElement;
  }
}
