import { LitElement, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { matIconStyles } from './mat-icon.styles';

@customElement('mat-icon')
export class MatIconElement extends LitElement {
  static styles = [matIconStyles];
  @property({ type: String })
  icon: string = '';

  render() {
    return html`
      <div class="mat-icon-container">
        <span class="mat-icon">${this.icon}</span>
      </div>
    `;
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'mat-icon': MatIconElement;
  }
}
