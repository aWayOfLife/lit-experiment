import { css } from 'lit';

export const matIconStyles = css`
  :host {
    display: inline-block;
    vertical-align: middle;
    width: 1em;
    height: 1em;
    user-select: none;
  }
  .mat-icon-container {
    position: relative;
    width: 1em;
    height: 1em;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .mat-icon {
    font-family: 'Material Icons Round', 'Material Icons';
    font-weight: normal;
    font-style: normal;
    font-size: 1em;
    line-height: 1;
    letter-spacing: normal;
    text-transform: none;
    display: inline-block;
    width: 100%;
    height: 100%;
    white-space: nowrap;
    word-wrap: normal;
    direction: ltr;
    -webkit-font-smoothing: antialiased;
  }
`;
