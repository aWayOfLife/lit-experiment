import { LitElement, html, css } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import moment from 'moment';
import { AudioMessageData, BottomNavMenuItem, SlotViewerData } from './elements/elements';
import { add, startOfMonth, startOfToday } from 'date-fns';

@customElement('showcase-element')
export class ShowcaseElement extends LitElement {
  static styles = css`
    :host {
      width: 100%;
      display: grid;
      place-items: center;
    }
    .showcase-wrapper {
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      gap: 36px;
      padding: 8px;
    }
    .showcase-introduction .showcase-title {
      font-size: 36px;
      margin-bottom: 16px;
    }
    .showcase-introduction .showcase-description {
      font-size: 16px;
      color: #7d7d7d;
    }
    .showcase {
      display: flex;
      flex-wrap: wrap;
      gap: 28px;
      justify-content: center;
    }
    .showcase-item {
      display: flex;
      flex-direction: column;
      justify-content: space-between;
      gap: 36px;
    }
    @media only screen and (max-width: 1200px) {
      .showcase-wrapper {
        padding: 20px;
      }
    }
    @media only screen and (max-width: 425px) {
      .showcase-introduction {
        width: 360px;
        margin: auto;
        margin-top: 40px;
      }
    }
  `;

  connectedCallback() {
    super.connectedCallback();
    const firstDayOfMonth = startOfMonth(startOfToday());
    this.activityDays = [add(firstDayOfMonth, { days: 4 }), add(firstDayOfMonth, { days: 5 }), add(firstDayOfMonth, { days: 14 }), add(firstDayOfMonth, { days: 19 }), add(firstDayOfMonth, { days: 20 })];
  }

  @state()
  activityDays: Date[] = [];

  @state()
  bottomNavMenuItems: BottomNavMenuItem[] = [
    {
      icon: 'door_sliding',
      isActive: true,
    },
    {
      icon: 'shopping_basket',
      isActive: false,
    },
    {
      icon: 'margin',
      isActive: false,
    },
    {
      icon: 'dns',
      isActive: false,
    },
  ];

  slotViewerData: SlotViewerData = {
    date: moment(),
    slots: [true, true, false, true, false, true, true, false, true, false, false, true, true, true, true, false, true, false, true, true, true, false, false, true],
  };

  audioMessageData: AudioMessageData = {
    duration: 60,
    elapsed: 0,
    levels: [1, 1, 2, 5, 3, 2, 5, 4, 4, 6, 3, 7, 8, 4, 8, 6, 7, 6, 4, 2, 5, 4, 4, 6, 3, 7, 3, 1, 3, 2, 3, 5, 6, 8, 4, 3, 3, 5, 1, 2, 2, 2, 1, 1],
    isPlaying: false,
  } as AudioMessageData;

  imageUrl = 'https://images.pexels.com/photos/1065084/pexels-photo-1065084.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2';

  render() {
    return html`
      <div class="showcase-wrapper">
        <div class="showcase-introduction">
          <div class="showcase-title">Made with Lit</div>
          <div class="showcase-description">Lit components are built on top of web components, so the browser treats them exactly like built-in elements.</div>
        </div>
        <div class="showcase">
          <div class="showcase-item">
            <user-card name="Katrina Fox" @cardClick=${this._handleCardClick} @addClick=${this._handleAddClick} designation="Senior Frontend Developer" imageUrl=${this.imageUrl}></user-card>
          </div>
          <div class="showcase-item">
            <smart-calendar .activityDays=${this.activityDays}></smart-calendar>
          </div>
          <div class="showcase-item">
            <slots-viewer .slotViewerData=${this.slotViewerData}></slots-viewer>
            <audio-message @playToggle=${this._handlePlayToggle} .audioMessageData=${this.audioMessageData}></audio-message>
            <bottom-nav .menuItems=${this.bottomNavMenuItems} @bottomNavActiveMenuItemChange=${this._handleBottomNavActiveMenuItemChange}></bottom-nav>
          </div>
        </div>
      </div>
    `;
  }

  private _handlePlayToggle(event: CustomEvent) {
    console.log('playToggle', event);
  }

  private _handleCardClick(event: Event) {
    console.log('card click', event);
  }

  private _handleAddClick(event: Event) {
    console.log('add click', event);
  }

  private _handleBottomNavActiveMenuItemChange(event: CustomEvent) {
    console.log('handleBottomNavActiveMenuItemChange', event);
    this.bottomNavMenuItems = [...this.bottomNavMenuItems.map((menuItem, i) => ({ ...menuItem, isActive: i === +event.detail.activeItemIndex }))];
  }
}

declare global {
  interface HTMLElementTagNameMap {
    'showcase-element': ShowcaseElement;
  }
}
