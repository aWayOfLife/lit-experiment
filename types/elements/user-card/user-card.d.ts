import { LitElement } from 'lit';
export declare class UserCardElement extends LitElement {
    static styles: import("lit").CSSResult[];
    imageUrl: string;
    name: string;
    designation: string;
    render(): import("lit-html").TemplateResult<1>;
    private _handleAddClick;
    private _handleCardClick;
}
declare global {
    interface HTMLElementTagNameMap {
        'user-card': UserCardElement;
    }
}
