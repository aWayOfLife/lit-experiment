import { LitElement } from 'lit';
import * as moment from 'moment';
export interface SlotViewerData {
    date?: moment.Moment;
    slots?: boolean[];
}
export declare class SlotsViewerElement extends LitElement {
    static styles: import("lit").CSSResult[];
    slotViewerData: SlotViewerData;
    render(): import("lit-html").TemplateResult<1>;
    private _getSlot;
}
declare global {
    interface HTMLElementTagNameMap {
        'slots-viewer': SlotsViewerElement;
    }
}
