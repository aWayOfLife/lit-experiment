import { LitElement } from 'lit';
export interface AudioMessageData {
    duration?: number;
    elapsed?: number;
    levels?: number[];
    isPlaying?: boolean;
}
export declare class AudioMessageElement extends LitElement {
    static styles: import("lit").CSSResult[];
    audioMessageData: AudioMessageData | undefined;
    timer: number | undefined;
    render(): import("lit-html").TemplateResult<1>;
    private _handlePlayToggle;
    private _getWave;
    private _getPercentageElapsed;
    private _getWaveHeightFromLevel;
}
declare global {
    interface HTMLElementTagNameMap {
        'audio-message': AudioMessageElement;
    }
}
