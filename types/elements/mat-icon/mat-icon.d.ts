import { LitElement } from 'lit';
export declare class MatIconElement extends LitElement {
    static styles: import("lit").CSSResult[];
    icon: string;
    render(): import("lit-html").TemplateResult<1>;
}
declare global {
    interface HTMLElementTagNameMap {
        'mat-icon': MatIconElement;
    }
}
