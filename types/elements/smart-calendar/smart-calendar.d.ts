import { LitElement, PropertyValues } from 'lit';
export interface SmartCalendarDate {
    day: Date;
    isSelectedDay: boolean;
    isActivityDay: boolean;
}
export declare class SmartCalendarElement extends LitElement {
    static styles: import("lit").CSSResult;
    firstDayOfMonth: Date;
    selectedDay: Date | undefined;
    smartCalendarDays: SmartCalendarDate[];
    activityDays: Date[];
    connectedCallback(): void;
    willUpdate(changedProperties: PropertyValues<this>): void;
    render(): import("lit-html").TemplateResult<1>;
    handleDayClick(event: CustomEvent): void;
    handlePreviousMonth(): void;
    handleNextMonth(): void;
    setSmartCalendarDays(): void;
    mapActivityDays(): void;
}
declare global {
    interface HTMLElementTagNameMap {
        'smart-calendar': SmartCalendarElement;
    }
}
