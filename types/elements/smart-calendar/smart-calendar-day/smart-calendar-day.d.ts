import { LitElement } from 'lit';
import { SmartCalendarDate } from '../smart-calendar';
export declare class SmartCalendarDayElement extends LitElement {
    static styles: import("lit").CSSResult;
    smartCalendarDay: SmartCalendarDate | undefined;
    render(): import("lit-html").TemplateResult<1>;
    getClasses(day: Date | undefined): string;
    handleDayClick(day: Date): void;
}
declare global {
    interface HTMLElementTagNameMap {
        'smart-calendar-day': SmartCalendarDayElement;
    }
}
