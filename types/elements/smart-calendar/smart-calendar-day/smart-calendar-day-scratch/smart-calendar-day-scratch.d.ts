import { LitElement } from 'lit';
export declare class SmartCalendarDayScratchElement extends LitElement {
    static styles: import("lit").CSSResult;
    render(): import("lit-html").TemplateResult<2>;
}
declare global {
    interface HTMLElementTagNameMap {
        'smart-calendar-day-scratch': SmartCalendarDayScratchElement;
    }
}
