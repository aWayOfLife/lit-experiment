import { LitElement } from 'lit';
export interface BottomNavMenuItem {
    icon: string;
    isActive: boolean;
}
export declare class BottomNavElement extends LitElement {
    static styles: import("lit").CSSResult[];
    menuItems: BottomNavMenuItem[];
    render(): import("lit-html").TemplateResult<1>;
    private _generateMenuItemTemplate;
    private _handleMenuItemClick;
}
declare global {
    interface HTMLElementTagNameMap {
        'bottom-nav': BottomNavElement;
    }
}
