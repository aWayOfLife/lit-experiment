import { LitElement } from 'lit';
export declare class ActionButtonElement extends LitElement {
    static styles: import("lit").CSSResult[];
    buttonIcon: string;
    render(): import("lit-html").TemplateResult<1>;
}
declare global {
    interface HTMLElementTagNameMap {
        'action-button': ActionButtonElement;
    }
}
