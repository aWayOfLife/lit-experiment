import { LitElement } from 'lit';
import { AudioMessageData, BottomNavMenuItem, SlotViewerData } from './elements/elements';
export declare class ShowcaseElement extends LitElement {
    static styles: import("lit").CSSResult;
    connectedCallback(): void;
    activityDays: Date[];
    bottomNavMenuItems: BottomNavMenuItem[];
    slotViewerData: SlotViewerData;
    audioMessageData: AudioMessageData;
    imageUrl: string;
    render(): import("lit-html").TemplateResult<1>;
    private _handlePlayToggle;
    private _handleCardClick;
    private _handleAddClick;
    private _handleBottomNavActiveMenuItemChange;
}
declare global {
    interface HTMLElementTagNameMap {
        'showcase-element': ShowcaseElement;
    }
}
